import SwiftCompilerPlugin
import SwiftSyntaxMacros

@main
struct TestMacroPlugin: CompilerPlugin {
  let providingMacros: [Macro.Type] = [
    StringifyMacro.self,
    AddBlocker.self,
    WarningMacro.self,
    WrapStoredPropertiesMacro.self,
    CaseDetectionMacro.self,
    MetaEnumMacro.self,
    CodableKey.self,
    CustomCodable.self,
    URLMacro.self
  ]
}
