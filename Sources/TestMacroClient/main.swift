import Foundation
import TestMacro

let x = 1
let y = 2
let z = 3

let (result, code) = #stringify(x + y)

print("\(code) = \(result)")

print(#URL("https://swift.org/"))

//let domain = "domain.com"
//print(#URL("https://\(domain)/api/path")) // error: #URL requires a static string literal
//print(#URL("https://not a url.com")) // error: Malformed url

@OptionSet<Int>
struct SundaeToppings {
    private enum Options: Int {
        case nuts
        case cherry
        case fudge
    }
}

let toppings: SundaeToppings = [.fudge, .cherry]

// Use the "wrapStoredProperties" macro to deprecate all of the stored
// properties.
@wrapStoredProperties(#"available(*, deprecated, message: "hands off my data")"#)
struct OldStorage {
  var x: Int
  var y: Int
}

// The deprecation warning below comes from the deprecation attribute
// introduced by @wrapStoredProperties on OldStorage.
_ = OldStorage(x: 5, y: 7).y


// "AddBlocker" complains about addition operations. We emit a warning
// so it doesn't block compilation.
print(#addBlocker(x * y + z))

#myWarning("remember to pass a string literal here")

// Uncomment to get an error out of the macro.
// let text = "oops"
// #myWarning(text)

@CaseDetection
enum Pet {
  case dog
  case cat(curious: Bool)
  case parrot
  case snake
}

let pet: Pet = .cat(curious: true)
print("Pet is dog: \(pet.isDog)")
print("Pet is cat: \(pet.isCat)")
print("Pet is snake: \(pet.isSnake)")


@CustomCodable
struct CustomCodableString: Codable {
  
  @CodableKey(name: "OtherName")
  var propertyWithOtherName: String
  
  var propertyWithSameName: Bool
 
  func randomFunction() {
    
  }
}

let json = """
{
  "OtherName": "Name",
  "propertyWithSameName": true
}

""".data(using: .utf8)!

let jsonDecoder = JSONDecoder()
let product = try jsonDecoder.decode(CustomCodableString.self, from: json)
print(product.propertyWithOtherName)

// `@MetaEnum` adds a nested enum called `Meta` with the same cases, but no
// associated values/payloads. Handy for e.g. describing a schema.
@MetaEnum enum Value {
  case integer(Int)
  case text(String)
  case boolean(Bool)
  case null
}

let v = Value.integer(42)
print(Value.Meta(v) == .integer)

