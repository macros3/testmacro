// The Swift Programming Language
// https://docs.swift.org/swift-book

import Foundation

/// A macro that produces both a value and a string containing the
/// source code that generated the value. For example,
///
///     #stringify(x + y)
///
/// produces a tuple `(x + y, "x + y")`.
@freestanding(expression)
public macro stringify<T>(_ value: T) -> (T, String) = #externalMacro(module: "TestMacroMacros", type: "StringifyMacro")


/// A macro that produces an unwrapped URL in case of a valid input URL.
/// For example,
///
///     #URL("https://www.avanderlee.com")
///
/// produces an unwrapped `URL` if the URL is valid. Otherwise, it emits a compile-time error.
@freestanding(expression)
public macro URL(_ stringLiteral: String) -> URL = #externalMacro(module: "TestMacroMacros", type: "URLMacro")


@attached(member, names: arbitrary)
@attached(extension, conformances: OptionSet)
public macro OptionSet<RawType>() = #externalMacro(module: "SwiftMacros", type: "OptionSetMacro")

/// Apply the specified attribute to each of the stored properties within the
/// type or member to which the macro is attached. The string can be
/// any attribute (without the `@`).
@attached(memberAttribute)
public macro wrapStoredProperties(_ attributeName: String) = #externalMacro(module: "TestMacroMacros", type: "WrapStoredPropertiesMacro")

/// Macro that produces a warning on "+" operators within the expression, and
/// suggests changing them to "-".
@freestanding(expression)
public macro addBlocker<T>(_ value: T) -> T = #externalMacro(module: "TestMacroMacros", type: "AddBlocker")

/// Macro that produces a warning, as a replacement for the built-in
/// #warning("...").
@freestanding(expression)
public macro myWarning(_ message: String) = #externalMacro(module: "TestMacroMacros", type: "WarningMacro")

/// Add computed properties named `is<Case>` for each case element in the enum.
@attached(member, names: arbitrary)
public macro CaseDetection() = #externalMacro(module: "TestMacroMacros", type: "CaseDetectionMacro")

@attached(peer, names: overloaded)
public macro CodableKey(name: String) = #externalMacro(module: "TestMacroMacros", type: "CodableKey")

@attached(member, names: named(CodingKeys))
public macro CustomCodable() = #externalMacro(module: "TestMacroMacros", type: "CustomCodable")

@attached(member, names: named(Meta))
public macro MetaEnum() = #externalMacro(module: "TestMacroMacros", type: "MetaEnumMacro")
